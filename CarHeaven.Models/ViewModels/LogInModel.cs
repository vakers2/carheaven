﻿namespace CarHeaven.Models.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class LogInModel
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}
