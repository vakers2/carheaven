﻿namespace CarHeaven.Models.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class UserAdsModel
    {
        public int Id { get; set; }

        public int Page { get; set; }
    }
}
