﻿namespace CarHeaven.Models.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using CarHeaven.Common;

    public class ChangeStatusModel
    {
        public List<int> UserIds { get; set; }

        public UserStatus Status { get; set; }
    }
}
