﻿namespace CarHeaven.Models.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class CarModelModel
    {
        public int CarModelId { get; set; }

        public string CarModelName { get; set; }
    }
}
