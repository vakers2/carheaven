﻿namespace CarHeaven.Models.ViewModels
{
    public class CarTypeModel
    {
        public int CarTypeId { get; set; }

        public string CarTypeName { get; set; }
    }
}
