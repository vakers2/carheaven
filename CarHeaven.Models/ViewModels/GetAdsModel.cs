﻿namespace CarHeaven.Models.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using CarHeaven.Common;

    public class GetAdsModel
    {
        public int? BrandId { get; set; }

        public int? CarModelId { get; set; }

        public int? CarTypeId { get; set; }

        public int? YearFrom { get; set; }

        public int? YearTo { get; set; }

        public int? PriceFrom { get; set; }

        public int? PriceTo { get; set; }

        public int? Page { get; set; }

        public SortFieldType? SortType { get; set; }

        public SortDirectionTypes? SortDirection { get; set; }
    }
}
