﻿namespace CarHeaven.Models.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using CarHeaven.Common;

    public class CheckUserModel
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public bool IsEmailConfirmed { get; set; }

        public Role RoleId { get; set; }

        public UserStatus Status { get; set; }
    }
}
