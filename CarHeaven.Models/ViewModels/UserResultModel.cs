﻿namespace CarHeaven.Models.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using CarHeaven.Common;

    public class UserResultModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public int UserId { get; set; }

        public int AdsCount { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? LastLoginDate { get; set; }

        public Role Role { get; set; }

        public UserStatus Status { get; set; }
    }
}
