﻿namespace CarHeaven.Models.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using CarHeaven.Common;

    public class GetUsersModel
    {
        public SortDirectionTypes? SortDirection { get; set; }

        public int CurrentPage { get; set; }

        public string SearchText { get; set; }

        public Role? Role { get; set; }
    }
}
