﻿namespace CarHeaven.Models.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class UsersSearchResultModel
    {
        public List<UserResultModel> SearchResult { get; set; }

        public int SearchResultCount { get; set; }
    }
}
