﻿namespace CarHeaven.Models.ViewModels
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using CarHeaven.Common;

    public class PostAdModel
    {
        [Required]
        public int CarModelId { get; set; }

        [MaxLength(128)]
        public string Description { get; set; }

        [Required]
        public int CarYear { get; set; }

        [Required]
        public int CarCost { get; set; }

        [Required]
        public FuelTypes FuelType { get; set; }

        [Required]
        public double Engine { get; set; }

        [Required]
        public int Mileage { get; set; }

        [Required]
        public string CarColor { get; set; }

        [Required]
        public TransmissionTypes Transmission { get; set; }

        public List<string> Images { get; set; }
    }
}
