﻿namespace CarHeaven.Models.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class UpdateAdModel : PostAdModel
    {
        public int? AdvertismentId { get; set; }
    }
}
