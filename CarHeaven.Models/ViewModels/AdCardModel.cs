namespace CarHeaven.Models
{
    using System;
    using System.Collections.Generic;

    public class AdCardModel
    {
        public int AdId { get; set; }

        public int UserId { get; set; }

        public string Brand { get; set; }

        public string CarModel { get; set; }

        public string CarType { get; set; }

        public string Owner { get; set; }

        public string Description { get; set; }

        public int CarYear { get; set; }

        public double CarCost { get; set; }

        public string FuelType { get; set; }

        public double Engine { get; set; }

        public int Mileage { get; set; }

        public string CarColor { get; set; }

        public string Transmission { get; set; }

        public string PhoneNumber { get; set; }

        public List<string> Images { get; set; }

        public int Views { get; set; }

        public int TodayViews { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime EditDate { get; set; }
    }
}
