﻿namespace CarHeaven.Models.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class LinkModel
    {
        public string Guid { get; set; }

        public int UserId { get; set; }
    }
}
