﻿namespace CarHeaven.Models.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class BrandModel
    {
        public int BrandId { get; set; }

        public string BrandName { get; set; }
    }
}
