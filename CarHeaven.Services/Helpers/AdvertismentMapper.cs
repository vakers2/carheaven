﻿namespace CarHeaven.Services.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using CarHeaven.Entities;
    using CarHeaven.Entities.Entities;
    using CarHeaven.Models;
    using CarHeaven.Models.ViewModels;

    public static class AdvertismentMapper
    {
        public static Advertisment MapToAdvertisment(PostAdModel ad, CarModel model)
        {
            if (ad == null || model == null)
            {
                return null;
            }

            var advert = new Advertisment
            {
                CarYear = ad.CarYear,
                CarColor = ad.CarColor,
                CarCost = ad.CarCost,
                CarModel = model,
                Description = ad.Description,
                Engine = ad.Engine,
                ExpireDate = DateTime.UtcNow.AddYears(1),
                CreationDate = DateTime.UtcNow,
                Mileage = ad.Mileage,

                // Owner = ad.Owner,
                // PhoneNumber = "111-00-00"
            };

            return advert;
        }

        public static AdCardModel MapToAdCardModel(Advertisment ad)
        {
            if (ad == null)
            {
                return null;
            }

            return new AdCardModel()
            {
                AdId = ad.AdvertismentId,
                Brand = ad.CarModel.Brand.BrandName,
                CarModel = ad.CarModel.CarModelName,
                CarType = ad.CarModel.CarType.CarTypeName,
                CarColor = ad.CarColor,
                CarCost = ad.CarCost,
                CarYear = ad.CarYear,
                Description = ad.Description,
                Engine = ad.Engine,
                FuelType = ad.FuelType.ToString(),
                Mileage = ad.Mileage,
                Owner = ad.Owner.FirstName + " " + ad.Owner.LastName,
                Transmission = ad.Transmission.ToString(),
                PhoneNumber = ad.Owner.PhoneNumber,
                Images = ad.Images == null ? new List<string>() : ad.Images.Select(x => x.Link).ToList(),
                Views = ad.Views?.Count ?? 0,
                TodayViews = ad.Views?.Count(x => x.ViewDate.Day == DateTime.UtcNow.Day) ?? 0,
                CreationDate = ad.ExpireDate.Subtract(DateTime.UtcNow.AddYears(1) - DateTime.UtcNow),
                UserId = ad.Owner.UserId
            };
        }

        public static CarImage GetImages(string link)
        {
            return new CarImage
            {
                Link = link
            };
        }
    }
}
