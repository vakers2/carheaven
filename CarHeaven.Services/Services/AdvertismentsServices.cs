namespace CarHeaven.Services.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using CarHeaven.Entities;
    using CarHeaven.Models;
    using CarHeaven.Models.ViewModels;
    using CarHeaven.Repository.Interfaces.Interfaces;
    using CarHeaven.Services.Helpers;
    using CarHeaven.Services.Interfaces.Interfaces;

    public class AdvertismentsServices : IAdvertismentsServices
    {
        private readonly IAdvertismentsRepository repository;
        private readonly ICarsRepository carsRepository;
        private readonly IUsersRepository usersRepository;
        private readonly IViewsRepository viewsRepository;

        public AdvertismentsServices(IAdvertismentsRepository repository, ICarsRepository carsRepository, IUsersRepository usersRepository, IViewsRepository viewsRepository)
        {
            this.repository = repository;
            this.carsRepository = carsRepository;
            this.usersRepository = usersRepository;
            this.viewsRepository = viewsRepository;
        }

        public AdCardModel GetAd(int id)
        {
            return AdvertismentMapper.MapToAdCardModel(repository.GetAd(id));
        }

        public List<AdCardModel> GetUserAds(UserAdsModel userAds)
        {
            return repository.GetUserAds(userAds.Id, userAds.Page).Select(AdvertismentMapper.MapToAdCardModel).OrderByDescending(x => x.EditDate).ToList();
        }

        public int GetUserAdsNumber(int id)
        {
            return repository.GetUserAdsCount(id);
        }

        public void CreateAd(PostAdModel ad, string email)
        {
            var advertisment = Mapper.Map<Advertisment>(ad);
            advertisment.Owner = usersRepository.GetUser(email);
            advertisment.Images = ad.Images.Select(AdvertismentMapper.GetImages).ToList();
            advertisment.CarModel = carsRepository.GetModelById(ad.CarModelId);
            advertisment.CreationDate = DateTime.UtcNow;
            advertisment.ExpireDate = DateTime.UtcNow.AddYears(1);
            repository.InsertAd(advertisment);
        }

        public void UpdateAd(UpdateAdModel ad, string email)
        {
            var advertisment = Mapper.Map<Advertisment>(ad);
            advertisment.Owner = usersRepository.GetUser(email);
            advertisment.Images = ad.Images.Select(AdvertismentMapper.GetImages).ToList();
            advertisment.CarModel = carsRepository.GetModelById(ad.CarModelId);
            advertisment.ExpireDate = DateTime.UtcNow.AddYears(1);
            repository.UpdateAd(advertisment);
        }

        public bool DeleteAd(int id)
        {
            var advert = repository.GetAd(id);
            if (advert == null)
            {
                return false;
            }

            repository.DeleteAd(advert);

            return true;
        }

        public int GetSearchAdsNumber(GetAdsModel adsInfo)
        {
            adsInfo.YearTo = adsInfo.YearTo ?? DateTime.Now.Year;
            adsInfo.YearFrom = adsInfo.YearFrom ?? 0;

            return repository.GetSearchAdsCount(adsInfo);
        }

        public List<AdCardModel> SearchAds(GetAdsModel adsInfo)
        {
            adsInfo.YearTo = adsInfo.YearTo ?? DateTime.Now.Year;
            adsInfo.YearFrom = adsInfo.YearFrom ?? 0;
            adsInfo.Page = adsInfo.Page ?? 0;

            return repository.GetSearchAds(adsInfo).Select(AdvertismentMapper.MapToAdCardModel).ToList();
        }
    }
}
