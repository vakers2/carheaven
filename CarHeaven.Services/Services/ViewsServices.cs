﻿namespace CarHeaven.Services.Services
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using CarHeaven.Repository.Interfaces.Interfaces;
    using CarHeaven.Repository.Repository;
    using CarHeaven.Services.Interfaces.Interfaces;

    public class ViewsServices : IViewsServices
    {
        private readonly IViewsRepository viewsRepository;

        public ViewsServices(IViewsRepository viewsRepository)
        {
            this.viewsRepository = viewsRepository;
        }

        public void CreateView(int id)
        {
            viewsRepository.InsertView(id);
        }
    }
}
