﻿namespace CarHeaven.Services.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using AutoMapper;
    using CarHeaven.Models.ViewModels;
    using CarHeaven.Repository.Interfaces.Interfaces;
    using CarHeaven.Services.Interfaces.Interfaces;

    public class CarsServices : ICarsServices
    {
        private readonly ICarsRepository repository;

        public CarsServices(ICarsRepository repository)
        {
            this.repository = repository;
        }

        public List<BrandModel> GetAllBrands()
        {
            return Mapper.Map<List<BrandModel>>(repository.GetAllBrands());
        }

        public List<CarModelModel> GetModels(int brandId)
        {
            return Mapper.Map<List<CarModelModel>>(repository.GetModels(brandId).OrderBy(x => x.CarModelName).ToList());
        }

        public List<CarTypeModel> GetTypes()
        {
            return Mapper.Map<List<CarTypeModel>>(repository.GetAllTypes());
        }
    }
}
