﻿namespace CarHeaven.Services.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using AutoMapper;
    using CarHeaven.Common;
    using CarHeaven.Entities.Entities;
    using CarHeaven.Models.ViewModels;
    using CarHeaven.Repository.Interfaces.Interfaces;
    using CarHeaven.Services.Interfaces.Interfaces;

    public class UsersServices : IUsersServices
    {
        private readonly IUsersRepository repository;
        private readonly SHA256 shaM;

        public UsersServices(IUsersRepository repository)
        {
            shaM = new SHA256Managed();
            this.repository = repository;
        }

        public LinkModel CreateUser(CreateUserModel createUser)
        {
            createUser.Password = Encoding.ASCII.GetString(shaM.ComputeHash(Encoding.ASCII.GetBytes(createUser.Password)));
            var user = Mapper.Map<User>(createUser);
            if (user.RoleId == Role.User)
            {
                user.Status = UserStatus.AwaitingEmailConfirmation;
            }

            return Mapper.Map<LinkModel>(repository.CreateUser(user));
        }

        public CreateUserModel GetUser(int userId)
        {
            return Mapper.Map<CreateUserModel>(repository.GetUser(userId));
        }

        public CreateUserModel GetUser(string email)
        {
            return Mapper.Map<CreateUserModel>(repository.GetUser(email));
        }

        public UserResultModel GetUserBasicInfo(string email)
        {
            return Mapper.Map<UserResultModel>(repository.GetUser(email));
        }

        public UsersSearchResultModel GetAllUsersInfo(GetUsersModel info)
        {
            return new UsersSearchResultModel() { SearchResult = repository.GetAllUsers(info).Select(Mapper.Map<UserResultModel>).ToList(), SearchResultCount = repository.GetAllUsersCount(info) };
        }

        public CheckUserModel GetUserInfo(string email)
        {
            return Mapper.Map<CheckUserModel>(repository.GetUser(email));
        }

        public CheckUserModel GetUserByGuid(string guid)
        {
            return Mapper.Map<CheckUserModel>(repository.GetUserByGuid(guid));
        }

        public bool ConfirmEmail(string guid)
        {
            var link = repository.GetLink(guid);
            if (link != null && !link.User.IsEmailConfirmed && link.ExpireDate >= DateTime.UtcNow)
            {
                link.User.IsEmailConfirmed = true;
                link.User.Status = UserStatus.Active;
                link.ExpireDate = DateTime.UtcNow;
                repository.ConfirmEmail(link);

                return true;
            }

            return false;
        }

        public bool Exist(string email)
        {
            return repository.GetUser(email) != null;
        }

        public LinkModel RenewLink(string guid)
        {
            var link = repository.GetLink(guid);
            if (link == null)
            {
                return null;
            }

            link.Guid = Guid.NewGuid().ToString();
            link.ExpireDate = DateTime.UtcNow.AddDays(7);

            return Mapper.Map<LinkModel>(repository.RenewLink(link));
        }

        public void CheckUpAuthorization(string email)
        {
            repository.CheckUpAuthorization(email);
        }

        public bool UpdateUser(UpdateUserModel userInfo, string originalEmail)
        {
            var user = repository.GetUser(originalEmail);
            user.FirstName = userInfo.FirstName;
            user.LastName = userInfo.LastName;
            user.Email = userInfo.Email;
            user.PhoneNumber = userInfo.PhoneNumber;

            return repository.UpdateUser(user);
        }

        public void ChangeUsersStatus(ChangeStatusModel statusInfo)
        {
            foreach (var id in statusInfo.UserIds)
            {
                var user = repository.GetUser(id);
                user.Status = statusInfo.Status;
                user.IsEmailConfirmed = true;
                repository.UpdateUser(user);
            }
        }
    }
}
