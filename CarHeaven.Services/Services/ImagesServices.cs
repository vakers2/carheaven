﻿namespace CarHeaven.Services.Services
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Threading.Tasks;
    using Amazon;
    using Amazon.S3;
    using Amazon.S3.Model;
    using CarHeaven.Services.Interfaces.Interfaces;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Http.Internal;

    public class ImagesServices : IImagesServices
    {
        public async Task<string> UploadImageAsync(FormFile image, string accessKey, string secretKey, string bucketName)
        {
            var client = new AmazonS3Client(accessKey, secretKey, RegionEndpoint.EUCentral1);
            var key = Guid.NewGuid().ToString();

            using (var memoryStream = new MemoryStream())
            {
                image.CopyTo(memoryStream);

                PutObjectRequest putRequest = new PutObjectRequest
                {
                    BucketName = bucketName,
                    Key = key,
                    InputStream = memoryStream,
                    CannedACL = S3CannedACL.PublicRead
                };

                PutObjectResponse response = await client.PutObjectAsync(putRequest).ConfigureAwait(false);
            }

            return "https://s3.eu-central-1.amazonaws.com/" + bucketName + "/" + key;
        }
    }
}
