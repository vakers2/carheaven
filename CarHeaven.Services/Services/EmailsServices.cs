﻿namespace CarHeaven.Services.Services
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using CarHeaven.Services.Interfaces.Interfaces;
    using CarHeaven.Services.Options;
    using Microsoft.Extensions.Options;
    using SendGrid;
    using SendGrid.Helpers.Mail;

    public class EmailsServices : IEmailsServices
    {
        public EmailsServices(IOptions<AuthMessageSenderOptions> optionsAccessor)
        {
            Options = optionsAccessor.Value;
        }

        public AuthMessageSenderOptions Options { get; }

        public Task SendEmailAsync(string email, string subject, string message, string serviceEmail, string owner)
        {
            return ExecuteAsync(Options.SendGridKey, subject, message, email, serviceEmail, owner);
        }

        public Task ExecuteAsync(string apiKey, string subject, string message, string email, string serviceEmail, string owner)
        {
            var client = new SendGridClient(apiKey);
            var msg = new SendGridMessage()
            {
                From = new EmailAddress(serviceEmail, owner),
                Subject = subject,
                PlainTextContent = message,
                HtmlContent = message
            };
            msg.AddTo(new EmailAddress(email));

            msg.TrackingSettings = new TrackingSettings
            {
                ClickTracking = new ClickTracking { Enable = false }
            };

            return client.SendEmailAsync(msg);
        }
    }
}
