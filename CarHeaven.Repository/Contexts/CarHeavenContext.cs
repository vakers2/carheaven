namespace CarHeaven.Repository.Contexts
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using CarHeaven.Entities;
    using CarHeaven.Entities.Entities;
    using CarHeaven.Models;
    using Microsoft.EntityFrameworkCore;

    public class CarHeavenContext : DbContext
    {
        public CarHeavenContext(DbContextOptions<CarHeavenContext> options)
            : base(options)
        {
        }

        public DbSet<Brand> Brands { get; set; }

        public DbSet<CarModel> Models { get; set; }

        public DbSet<CarType> CarTypes { get; set; }

        public DbSet<Advertisment> Ads { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<RegistrationLink> RegistrationLinks { get; set; }

        public DbSet<CarImage> CarImages { get; set; }

        public DbSet<View> Views { get; set; }
    }
}
