﻿namespace CarHeaven.Repository.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using CarHeaven.Common;
    using CarHeaven.CsvReader;
    using CarHeaven.Entities;
    using CarHeaven.Entities.Entities;
    using CarHeaven.Models;
    using CarHeaven.Models.ViewModels;
    using CarHeaven.Repository.Contexts;
    using CarHeaven.Repository.Interfaces;
    using CarHeaven.Repository.Interfaces.Interfaces;
    using Microsoft.EntityFrameworkCore;

    public class AdvertismentsRepository : IAdvertismentsRepository
    {
        private const int AdsOnPage = 10;

        private readonly CarHeavenContext context;

        public AdvertismentsRepository(CarHeavenContext context)
        {
            this.context = context;
        }

        public void InsertAd(Advertisment ad)
        {
            context.Ads.Add(ad);
            context.SaveChanges();
        }

        public Advertisment GetAd(int id)
        {
            return context.Ads.Include(x => x.CarModel).Include(x => x.CarModel.Brand).Include(x => x.CarModel.CarType)
                .Include(x => x.Images).Include(x => x.Owner).Include(x => x.Views).SingleOrDefault(x => x.AdvertismentId == id);
        }

        public List<Advertisment> GetUserAds(int id, int page)
        {
            return context.Ads.Include(x => x.CarModel).Include(x => x.CarModel.Brand).Include(x => x.CarModel.CarType)
                .Include(x => x.Images).Include(x => x.Owner).Include(x => x.Views).Where(x => x.Owner.UserId == id).OrderByDescending(x => x.ExpireDate).Skip((page - 1) * AdsOnPage).Take(AdsOnPage).ToList();
        }

        public int GetUserAdsCount(int id)
        {
            return context.Ads.Include(x => x.CarModel).Include(x => x.CarModel.Brand)
                .Include(x => x.CarModel.CarType).Include(x => x.Images).Include(x => x.Owner).Include(x => x.Views).Count(x => x.Owner.UserId == id);
        }

        public void UpdateAd(Advertisment advert)
        {
            context.CarImages.RemoveRange(context.CarImages.Where(x => x.AdvertismentId == advert.AdvertismentId));
            context.Ads.Update(advert);
            context.SaveChanges();
        }

        public void DeleteAd(Advertisment advert)
        {
            context.Ads.Remove(advert);
            context.SaveChanges();
        }

        public List<Advertisment> GetSearchAds(GetAdsModel adsInfo)
        {
            return SortAdvertisments(SearchAds(adsInfo), adsInfo).Skip((adsInfo.Page.Value - 1) * AdsOnPage).Take(AdsOnPage).ToList();
        }

        public int GetSearchAdsCount(GetAdsModel adsInfo)
        {
            return SearchAds(adsInfo).Count();
        }

        private IQueryable<Advertisment> SearchAds(GetAdsModel adsInfo)
        {
            var ads = context.Ads.Include(x => x.CarModel).Include(x => x.CarModel.Brand)
                .Include(x => x.CarModel.CarType).Include(x => x.Images).Include(x => x.Owner).AsQueryable();
            if (adsInfo.BrandId.HasValue)
            {
                ads = ads.Where(x => x.CarModel.Brand.BrandId == adsInfo.BrandId);
            }

            if (adsInfo.CarModelId.HasValue)
            {
                ads = ads.Where(x => x.CarModel.CarModelId == adsInfo.CarModelId);
            }

            if (adsInfo.CarTypeId.HasValue)
            {
                ads = ads.Where(x => x.CarModel.CarType.CarTypeId == adsInfo.CarTypeId);
            }

            ads = ads.Where(x => x.CarYear >= adsInfo.YearFrom && x.CarYear <= adsInfo.YearTo);

            if (adsInfo.PriceFrom.HasValue)
            {
                ads = ads.Where(x => x.CarCost >= adsInfo.PriceFrom);
            }

            if (adsInfo.PriceTo.HasValue)
            {
                ads = ads.Where(x => x.CarCost <= adsInfo.PriceTo);
            }

            return ads;
        }

        private IQueryable<Advertisment> SortAdvertisments(IQueryable<Advertisment> ads, GetAdsModel adsInfo)
        {
            switch (adsInfo.SortType)
            {
                case SortFieldType.EditDate:
                    ads = adsInfo.SortDirection == SortDirectionTypes.Down
                        ? ads.OrderByDescending(x => x.ExpireDate)
                        : ads.OrderBy(x => x.ExpireDate);
                    break;
                case SortFieldType.Year:
                    ads = adsInfo.SortDirection == SortDirectionTypes.Down
                        ? ads.OrderByDescending(x => x.CarYear)
                        : ads.OrderBy(x => x.CarYear);
                    break;
                case SortFieldType.Price:
                    ads = adsInfo.SortDirection == SortDirectionTypes.Down
                        ? ads.OrderByDescending(x => x.CarCost)
                        : ads.OrderBy(x => x.CarCost);
                    break;
            }

            return ads;
        }
    }
}
