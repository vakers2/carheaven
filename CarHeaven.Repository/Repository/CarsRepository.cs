﻿namespace CarHeaven.Repository.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using CarHeaven.Entities;
    using CarHeaven.Repository.Contexts;
    using CarHeaven.Repository.Interfaces.Interfaces;
    using Microsoft.EntityFrameworkCore;

    public class CarsRepository : ICarsRepository
    {
        private readonly CarHeavenContext context;

        public CarsRepository(CarHeavenContext context)
        {
            this.context = context;
        }

        public List<Brand> GetAllBrands()
        {
            return context.Brands.ToList();
        }

        public List<CarModel> GetModels(int brandId)
        {
            return context.Models.Include(x => x.Brand).Where(x => x.BrandId == brandId).ToList();
        }

        public List<CarType> GetAllTypes()
        {
            return context.CarTypes.ToList();
        }

        public CarModel GetModelById(int id)
        {
            return context.Models.SingleOrDefault(x => x.CarModelId == id);
        }
    }
}
