﻿namespace CarHeaven.Repository.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CarHeaven.Entities;
    using CarHeaven.Repository.Contexts;
    using CarHeaven.Repository.Interfaces.Interfaces;

    public class GeneratingRepository : IGeneratingRepository
    {
        private readonly CarHeavenContext context;

        public GeneratingRepository(CarHeavenContext context)
        {
            this.context = context;
        }

        public void SeedAds()
        {
            Random rand = new Random();
            var colors = new[] { "Green", "Red", "Blue", "Yellow", "Purple", "White", "Black" };
            var transmissions = new[] { "Automatic", "Manual", "Automated Manual" };
            var fuelType = new[] { "Gasoline", "Diesel" };

            List<Advertisment> newAds = new List<Advertisment>();
            for (int i = 0; i < 1000; i++)
            {
                newAds.Add(new Advertisment
                {
                    CarYear = rand.Next(1970, 2018),
                    CarCost = rand.Next(1000, 100000),
                    Engine = rand.Next(6, 18),
                    ExpireDate = DateTime.UtcNow.AddMonths(3),

                    // FuelType = fuelType[rand.Next(0, 1)],
                    Mileage = rand.Next(100, 100000),

                    // TransmissionTypes = transmissions[rand.Next(0, 2)],
                    CarColor = colors[rand.Next(0, 6)],

                    // PhoneNumber = rand.Next(100, 999) + "-" + rand.Next(10, 99) + "-" + rand.Next(10, 99),
                    CarModel = context.Models.OrderBy(x => Guid.NewGuid()).Take(1).First(),

                    // Owner = "Vlados"
                });
            }

            context.Ads.AddRange(newAds);
            context.SaveChanges();
        }

        public CarModel CreateCar(string brand, string model, string carType)
        {
            Brand brandObj;
            CarModel carModelObj;
            CarType carTypeObj;
            if (!context.Brands.Any(x => x.BrandName == brand))
            {
                context.Brands.Add(brandObj = new Brand
                {
                    BrandName = brand
                });
            }
            else
            {
                brandObj = context.Brands.First(x => x.BrandName == brand);
            }

            if (!context.Models.Any(x => x.CarModelName == model))
            {
                context.Models.Add(carModelObj = new CarModel
                {
                    CarModelName = model,
                    Brand = brandObj
                });
            }
            else
            {
                carModelObj = context.Models.First(x => x.CarModelName == model);
            }

            if (!context.CarTypes.Any(x => x.CarTypeName == carType))
            {
                context.CarTypes.Add(carTypeObj = new CarType
                {
                    CarTypeName = carType,
                    Models = new List<CarModel>() { carModelObj }
                });
            }
            else
            {
                carTypeObj = context.CarTypes.First(x => x.CarTypeName == carType);
                carTypeObj.Models.Add(carModelObj);
            }

            context.SaveChanges();

            return carModelObj;
        }
    }
}
