﻿namespace CarHeaven.Repository.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using CarHeaven.Common;
    using CarHeaven.Entities.Entities;
    using CarHeaven.Models.ViewModels;
    using CarHeaven.Repository.Contexts;
    using CarHeaven.Repository.Interfaces.Interfaces;
    using Microsoft.EntityFrameworkCore;

    public class UsersRepository : IUsersRepository
    {
        private const int UsersOnPage = 10;

        private readonly CarHeavenContext context;

        public UsersRepository(CarHeavenContext context)
        {
            this.context = context;
        }

        public RegistrationLink CreateUser(User user)
        {
            user.DateCreated = DateTime.UtcNow;
            if (user.RoleId == Role.User)
            {
                var link = new RegistrationLink
                {
                    Guid = Guid.NewGuid().ToString(),
                    ExpireDate = DateTime.UtcNow.AddDays(7),
                    User = user
                };

                context.RegistrationLinks.Add(link);
                context.Users.Add(user);
                context.SaveChanges();

                return link;
            }

            context.Users.Add(user);
            context.SaveChanges();

            return null;
        }

        public RegistrationLink GetLink(string id)
        {
            return context.RegistrationLinks.Include(x => x.User).SingleOrDefault(x => x.Guid == id);
        }

        public User GetUser(int userId)
        {
            return context.Users.Include(x => x.Advertisments).SingleOrDefault(x => x.UserId == userId);
        }

        public List<User> GetAllUsers(GetUsersModel info)
        {
            if (string.IsNullOrEmpty(info.SearchText))
            {
                var users = SortUsers(context.Users.Include(x => x.Advertisments).ToList(), info.SortDirection.GetValueOrDefault())
                    .Skip((info.CurrentPage - 1) * UsersOnPage).Take(UsersOnPage);
                if (info.Role.HasValue)
                {
                    users = users.Where(x => x.RoleId == info.Role);
                }

                return users.ToList();
            }

            return SortUsers(context.Users.Include(x => x.Advertisments).Where(x => x.Email.Contains(info.SearchText)).ToList(), info.SortDirection.GetValueOrDefault()).Skip((info.CurrentPage - 1) * UsersOnPage).Take(UsersOnPage).ToList();
        }

        public int GetAllUsersCount(GetUsersModel info)
        {
            if (string.IsNullOrEmpty(info.SearchText))
            {
                return context.Users.ToList().Count;
            }

            return context.Users.Count(x => x.Email.Contains(info.SearchText));
        }

        public User GetUser(string email)
        {
            return context.Users.Include(x => x.Advertisments).SingleOrDefault(x => x.Email == email);
        }

        public User GetUserByGuid(string guid)
        {
            var link = context.RegistrationLinks.Include(x => x.User).SingleOrDefault(x => x.Guid == guid);

            return link?.User;
        }

        public RegistrationLink RenewLink(RegistrationLink link)
        {
            context.RegistrationLinks.Update(link);
            context.SaveChanges();

            return link;
        }

        public void ConfirmEmail(RegistrationLink link)
        {
            context.RegistrationLinks.Update(link);
            context.SaveChanges();
        }

        public void CheckUpAuthorization(string email)
        {
            context.Users.First(x => x.Email == email).LastLoginDate = DateTime.UtcNow;
            context.SaveChanges();
        }

        public bool UpdateUser(User user)
        {
            context.Users.Update(user);
            context.SaveChanges();

            return true;
        }

        private static IEnumerable<User> SortUsers(IEnumerable<User> users, SortDirectionTypes direction)
        {
            switch (direction)
            {
                case SortDirectionTypes.Down:
                    return users.OrderByDescending(x => x.RoleId).ToList();
                case SortDirectionTypes.Up:
                    return users.OrderBy(x => x.RoleId).ToList();
                default:
                    return users;
            }
        }
    }
}
