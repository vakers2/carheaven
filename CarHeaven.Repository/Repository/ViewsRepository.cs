﻿namespace CarHeaven.Repository.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using CarHeaven.Entities.Entities;
    using CarHeaven.Repository.Contexts;
    using CarHeaven.Repository.Interfaces.Interfaces;

    public class ViewsRepository : IViewsRepository
    {
        private readonly CarHeavenContext context;

        public ViewsRepository(CarHeavenContext context)
        {
            this.context = context;
        }

        public void InsertView(int id)
        {
            context.Views.Add(new View { AdvertismentId = id, ViewDate = DateTime.UtcNow });
            context.SaveChanges();
        }
    }
}
