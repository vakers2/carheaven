// <copyright file="AuthorizationHelper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarHeaven.Web.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading.Tasks;
    using CarHeaven.Common;
    using CarHeaven.Models.ViewModels;
    using CarHeaven.Repository.Interfaces.Interfaces;
    using CarHeaven.Services.Interfaces.Interfaces;
    using Microsoft.AspNetCore.Authentication;
    using Microsoft.AspNetCore.Authentication.Cookies;
    using Microsoft.AspNetCore.Http;

    public static class AuthorizationHelper
    {
        private static readonly SHA256 ShaM = new SHA256CryptoServiceProvider();

        public static string HashPassword(string password)
        {
            return Encoding.ASCII.GetString(ShaM.ComputeHash(Encoding.ASCII.GetBytes(password)));
        }

        public static async Task<bool> SignInAsync(HttpContext context, LogInModel user, CheckUserModel dbUser)
        {
            if (dbUser == null || HashPassword(user.Password) != dbUser.Password || dbUser.Status == UserStatus.Blocked || dbUser.RoleId != Role.User)
            {
                return false;
            }

            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, user.Email),
            };

            var claimsIdentity = new ClaimsIdentity(
                claims, CookieAuthenticationDefaults.AuthenticationScheme);

            var authProperties = new AuthenticationProperties
            {
                IsPersistent = true
            };

            await context.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsIdentity),
                authProperties).ConfigureAwait(false);

            return true;
        }

        public static async Task<bool> SignInAsAdminAsync(HttpContext context, LogInModel user, CheckUserModel dbUser)
        {
            if (dbUser == null || HashPassword(user.Password) != dbUser.Password || dbUser.Status == UserStatus.Blocked || dbUser.RoleId == Role.User)
            {
                return false;
            }

            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, user.Email),
            };

            var claimsIdentity = new ClaimsIdentity(
                claims, CookieAuthenticationDefaults.AuthenticationScheme);

            var authProperties = new AuthenticationProperties
            {
                IsPersistent = true
            };

            await context.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsIdentity),
                authProperties).ConfigureAwait(false);

            return true;
        }

        public static async Task<bool> SignInAsync(HttpContext context, CheckUserModel user)
        {
            if (user == null)
            {
                return false;
            }

            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, user.Email),
            };

            var claimsIdentity = new ClaimsIdentity(
                claims, CookieAuthenticationDefaults.AuthenticationScheme);

            var authProperties = new AuthenticationProperties();

            await context.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsIdentity),
                authProperties).ConfigureAwait(false);

            return true;
        }

        public static async Task SignOutAsync(HttpContext context)
        {
            await context.SignOutAsync().ConfigureAwait(false);
        }
    }
}
