import Catalog from 'components/Catalog/Catalog'
import SignUp from 'components/Authorization/SignUp'
import SignIn from 'components/Authorization/SignIn'
import SignUpSuccess from 'components/Authorization/SignUpSuccess'
import EmailConfirmation from 'components/Authorization/EmailConfirmation'
import InvalidLink from 'components/Authorization/InvalidLink'
import NotFound from 'components/NotFound'
import UserPage from 'components/User/UserPage'
import AdCreationPage from 'components/Advertisment/AdCreator'
import AdvertPage from 'components/Advertisment/AdvertPage'
import UserAdverts from 'components/User/UserAdverts'
import UserManager from 'components/User/UserManager'

export const routeNames = {
  Home: 'home',
  Catalog: 'catalog',
  SignIn: 'sign-in',
  SignUp: 'sign-up',
  SignUpAdmin: 'admin',
  RegistrationSuccess: 'registration-success',
  EmailConfirmation: 'email-confirmation',
  InvalidLink: 'invalid-link',
  NotFound: '404',
  UserPage: 'user',
  AdCreationPage: 'create',
  AdvertPage: 'advert',
  UserAdverts: 'user-adverts',
  EditPage: 'edit',
  UserManager: 'manage'
}

export const routes = [
  { name: routeNames.Home, path: '/', component: Catalog, display: 'none', icon: '' },
  { name: routeNames.Catalog, path: '/catalog', component: Catalog, display: 'Catalog', icon: '' },
  { name: routeNames.SignIn, path: '/sign-in', component: SignIn, display: 'Sign-in', icon: '' },
  { name: routeNames.SignUp, path: '/sign-up', component: SignUp, display: 'Sign-up', icon: '' },
  { name: routeNames.SignUpAdmin, path: '/admin', component: SignIn, display: '', icon: '' },
  { name: routeNames.RegistrationSuccess, path: '/success', component: SignUpSuccess, display: 'none', icon: '' },
  { name: routeNames.EmailConfirmation, path: '/confirm-email', component: EmailConfirmation, display: 'none', icon: '' },
  { name: routeNames.InvalidLink, path: '/invalid-link', component: InvalidLink, display: 'none', icon: '' },
  { name: routeNames.NotFound, path: '/404', component: NotFound, display: 'none', icon: '' },
  { name: routeNames.UserPage, path: '/user', component: UserPage, display: 'none', icon: '' },
  { name: routeNames.AdCreationPage, path: '/create', alias: '/edit/:id', component: AdCreationPage, display: 'none', icon: '' },
  { name: routeNames.AdvertPage, path: '/advert/:id', component: AdvertPage, display: 'none', icon: '' },
  { name: routeNames.UserAdverts, path: '/user-adverts/:id', component: UserAdverts, display: 'none', icon: '' },
  { name: routeNames.EditPage, path: '/edit/:id', component: AdCreationPage, display: 'none', icon: '' },
  { name: routeNames.UserManager, path: '/manage', component: UserManager, display: 'none', icon: '' }
]
