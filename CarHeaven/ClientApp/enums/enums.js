export const enums = {
  Transmissions: [
    { Id: 0, Display: 'Automatic' },
    { Id: 1, Display: 'Manual' },
    { Id: 2, Display: 'Semi-automatic' }
  ],
  FuelTypes: [
    { Id: 0, Display: 'Gasoline' },
    { Id: 1, Display: 'Diesel' },
    { Id: 2, Display: 'Gas' },
    { Id: 3, Display: 'Electricity' }
  ],
  RoleTypes: [
    { Id: 1, Display: 'User' },
    { Id: 2, Display: 'Admin' },
    { Id: 3, Display: 'Super admin' }
  ],
  StatusTypes: [
    { Id: 0, Display: 'Active' },
    { Id: 2, Display: 'Blocked' }
  ],
  SortType: Object.freeze({
    EditDate: 0,
    Year: 1,
    Price: 2
  }),
  SortDirection: Object.freeze({
    Down: 0,
    Up: 1
  }),
  Role: Object.freeze({
    User: 1,
    Admin: 2,
    SuperAdmin: 3
  }),
  Status: Object.freeze({
    Active: 0,
    AwaitingEmailConfirmation: 1,
    Blocked: 2
  })
}
