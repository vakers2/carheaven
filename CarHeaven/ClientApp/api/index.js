import axios from 'axios'

axios.defaults.baseURL = '/'
axios.defaults.headers.post['Content-Type'] = 'application/json'

export default {
  home: {
    get: {
      getAll () {
        return axios.get('Home/GetAll')
      },
      getAllBrands () {
        return axios.get('Cars/GetAllBrands')
      },
      getModels (brand) {
        return axios.get('Cars/GetModels', {
          params: {
            brandId: brand
          }
        })
      },
      getCarTypes () {
        return axios.get('Cars/GetTypes')
      }
    }
  },
  advertisment: {
    get: {
      getAdvertisment (id) {
        return axios.get('Advertisments/GetAdvert', {
          params: {
            id
          }
        })
      },
      getUserAdvertismentsNumber (id) {
        return axios.get('Advertisments/GetUserAdsNumber', {
          params: {
            id
          }
        })
      }
    },
    delete: {
      deleteAd (id) {
        return axios.delete('Advertisments/DeleteAdvert', {
          params: {
            id
          }
        })
      }
    },
    post: {
      getSearchAds (page, brand, model, type, yearFrom, yearTo, priceFrom, priceTo, sortType, sortDirection) {
        var data = {
          BrandId: brand,
          CarModelId: model,
          CarTypeId: type,
          YearFrom: yearFrom,
          YearTo: yearTo,
          PriceFrom: priceFrom,
          PriceTo: priceTo,
          Page: page,
          SortType: sortType,
          SortDirection: sortDirection
        }
        return axios.post('Advertisments/SearchAds', data)
      },
      getSearchNumber (brand, model, type, yearFrom, yearTo, priceFrom, priceTo) {
        var data = {
          BrandId: brand,
          CarModelId: model,
          CarTypeId: type,
          YearFrom: yearFrom,
          YearTo: yearTo,
          PriceFrom: priceFrom,
          PriceTo: priceTo,
          Page: 1
        }
        return axios.post('Advertisments/GetSearchNumber', data)
      },
      getUserAdvertisments (id, page) {
        return axios.post('Advertisments/GetUserAds', {
          id,
          page
        })
      },
      createAd (data) {
        return axios.post('Advertisments/CreateAd', data)
      },
      uploadImage (data) {
        return axios.post('Advertisments/UploadImage', data)
      },
      updateAd (data) {
        return axios.post('Advertisments/UpdateAd', data)
      }
    }
  },
  user: {
    post: {
      updateUser (firstName, lastName, email, phoneNumber) {
        var data = {
          FirstName: firstName,
          LastName: lastName,
          Email: email,
          PhoneNumber: phoneNumber
        }

        return axios.post('Users/UpdateUserAsync', data)
      },
      getAllUsers (data) {
        return axios.post('Users/GetAllUsers', data)
      },
      changeUsersStatus (data) {
        return axios.post('Users/ChangeStatus', data)
      }
    }
  },
  authorization: {
    get: {
      checkUser () {
        return axios.get('Authorization/CheckAuthorization')
      },
      signOut () {
        return axios.get('Authorization/SignOutAsync')
      },
      renew (id) {
        return axios.get('Authorization/Renew?id=' + id)
      }
    },
    post: {
      registerUser (firstName, lastName, phoneNumber, email, password, role) {
        var data = {
          FirstName: firstName,
          LastName: lastName,
          PhoneNumber: phoneNumber,
          Email: email,
          Password: password,
          RoleId: role
        }

        return axios.post('Authorization/RegisterUser', data)
      },
      logInUser (email, password) {
        var data = {
          Email: email,
          Password: password
        }

        return axios.post('Authorization/LogIn', data)
      },
      logInAdmin (email, password) {
        var data = {
          Email: email,
          Password: password
        }

        return axios.post('Authorization/LogInAdmin', data)
      }
    }
  }
}
