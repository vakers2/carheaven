import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

// TYPES

const MAIN_SET_COUNTER = 'MAIN_SET_COUNTER'

const MAKE_NAME = 'MAKE_NAME'
const RESET_NAME = 'RESET_NAME'
const MAKE_ID = 'MAKE_ID'
const MAKE_ADMIN = 'MAKE_ADMIN'
const RESET_ADMIN = 'RESET_ADMIN'

// STATE
const state = {
  counter: 1,
  userName: '',
  userId: '',
  admin: false
}

// MUTATIONS
const mutations = {
  [MAIN_SET_COUNTER] (state, obj) {
    state.counter = obj.counter
  },
  [MAKE_NAME] (state, value) {
    state.userName = value
  },
  [RESET_NAME] (state) {
    state.userName = ''
    state.userId = ''
  },
  [MAKE_ID] (state, value) {
    state.userId = value
  },
  [MAKE_ADMIN] (state) {
    state.admin = true
  },
  [RESET_ADMIN] (state) {
    state.admin = false
  }
}

// ACTIONS
const actions = ({
  setCounter ({ commit }, obj) {
    commit(MAIN_SET_COUNTER, obj)
  }
})

export default new Vuex.Store({
  state,
  mutations,
  actions,
  getters: {
    isAdmin: (state) => {
      return state.admin
    },
    getUserName: (state) => {
      return state.userName
    },
    getUserId: (state) => {
      return state.userId
    },
    getYears: () => {
      var date = new Date().getFullYear() - 1970 + 1
      return Array.apply(0, Array(date))
        .map(function (element, index) {
          return index + 1970
        })
        .reverse()
    }
  }
})
