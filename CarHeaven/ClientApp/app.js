import Vue from 'vue'
import axios from 'axios'
import router from './router'
import store from './store'
import validation from './validation'
import { sync } from 'vuex-router-sync'
import App from 'components/app-root'
import { FontAwesomeIcon } from './icons'
import BootstrapVue from 'bootstrap-vue'

// Registration of global components
Vue.component('icon', FontAwesomeIcon)

Vue.prototype.$http = axios

Vue.use(BootstrapVue)

sync(store, router)

const app = new Vue({
  store,
  router,
  validation,
  BootstrapVue,
  ...App
})

export {
  app,
  router,
  store
}
