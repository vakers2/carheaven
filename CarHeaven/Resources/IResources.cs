namespace CarHeaven.Web
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Localization;

    public interface IResources
    {
        string EmailTitle { get; }

        string EmailContent { get; }
    }
}
