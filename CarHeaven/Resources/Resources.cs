namespace CarHeaven.Web
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Localization;

    public class Resources : IResources
    {
        private readonly IStringLocalizer<Resources> localizer;

        public Resources(IStringLocalizer<Resources> localizer) =>
            this.localizer = localizer;

        public string EmailTitle => GetString(nameof(EmailTitle));

        public string EmailContent => GetString(nameof(EmailContent));

        private string GetString(string name) =>
            localizer[name];
    }
}
