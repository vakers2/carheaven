namespace Vue
{
    using System;
    using AutoMapper;
    using CarHeaven.DependencyInjection;
    using CarHeaven.Entities;
    using CarHeaven.Entities.Entities;
    using CarHeaven.Models.ViewModels;
    using CarHeaven.Repository;
    using CarHeaven.Repository.Contexts;
    using CarHeaven.Services.Options;
    using CarHeaven.Web;
    using CarHeaven.Web.Config;
    using CarHeaven.Web.Helpers;
    using Microsoft.AspNetCore.Authentication.Cookies;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.SpaServices.Webpack;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.DependencyInjection.Extensions;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            CreateMaps();

            AppConfig.Init(Configuration);

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.Cookie.Name = "AuthorizationCookie";
                });

            services.Configure<AuthMessageSenderOptions>(Configuration);

            services.AddLocalization(o => o.ResourcesPath = "Resources");

            // Add framework services.
            services.AddMvc()
                .AddViewLocalization()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            var connection = AppConfig.ConnectionString;
            services.AddDbContext<CarHeavenContext>(
                options => options.UseSqlServer(connection, b => b.MigrationsAssembly("CarHeaven.Web")));

            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.TryAddSingleton<IResources, Resources>();

            DependencyInjection.InjectDependencies(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                // Webpack initialization with hot-reload.
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true,
                });
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Home", action = "Index" });
            });
        }

        private static void CreateMaps()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<PostAdModel, Advertisment>().ForMember(i => i.Images, m => m.Ignore());
                cfg.CreateMap<CreateUserModel, User>();
                cfg.CreateMap<CheckUserModel, User>();
                cfg.CreateMap<User, CreateUserModel>();
                cfg.CreateMap<User, UserResultModel>().ForMember(i => i.AdsCount, m => m.MapFrom(x => x.Advertisments.Count)).ForMember(i => i.Role, m => m.MapFrom(x => x.RoleId));
                cfg.CreateMap<LinkModel, RegistrationLink>();
                cfg.CreateMap<Brand, BrandModel>();
                cfg.CreateMap<CarModel, CarModelModel>();
                cfg.CreateMap<CarType, CarTypeModel>();
            });
        }
    }
}
