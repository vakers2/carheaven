namespace CarHeaven.Web.Config
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public static class AppConfig
    {
        private const string EmailSection = "EmailServiceInfo";

        private const string S3Section = "S3Info";

        public static string ServiceOwner { get; set; }

        public static string ServiceEmail { get; set; }

        public static string ConnectionString { get; set; }

        public static string BucketName { get; set; }

        public static string AWSAccessKey { get; set; }

        public static string AWSSecretKey { get; set; }

        public static void Init(IConfiguration config)
        {
            ServiceOwner = config.GetSection(EmailSection)[nameof(ServiceOwner)];
            ServiceEmail = config.GetSection(EmailSection)[nameof(ServiceEmail)];
            AWSAccessKey = config[nameof(AWSAccessKey)];
            AWSSecretKey = config[nameof(AWSSecretKey)];
            ConnectionString = config.GetConnectionString(nameof(ConnectionString));
            BucketName = config.GetSection(S3Section)[nameof(BucketName)];
        }
    }
}
