namespace CarHeaven.Web.Migrations
{
    using Microsoft.EntityFrameworkCore.Migrations;

    public partial class Fixentitytypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CarImage_Ads_AdvertismentId",
                table: "CarImages");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CarImage",
                table: "CarImages");

            migrationBuilder.DropColumn(
                name: "Owner",
                table: "Ads");

            migrationBuilder.DropColumn(
                name: "PhoneNumber",
                table: "Ads");

            migrationBuilder.RenameIndex(
                name: "IX_CarImage_AdvertismentId",
                table: "CarImages",
                newName: "IX_CarImages_AdvertismentId");

            migrationBuilder.AddColumn<string>(
                name: "PhoneNumber",
                table: "Users",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TransmissionTypes",
                table: "Ads",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FuelType",
                table: "Ads",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OwnerUserId",
                table: "Ads",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_CarImages",
                table: "CarImages",
                column: "CarImageId");

            migrationBuilder.CreateIndex(
                name: "IX_Ads_OwnerUserId",
                table: "Ads",
                column: "OwnerUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Ads_Users_OwnerUserId",
                table: "Ads",
                column: "OwnerUserId",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CarImages_Ads_AdvertismentId",
                table: "CarImages",
                column: "AdvertismentId",
                principalTable: "Ads",
                principalColumn: "AdvertismentId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Ads_Users_OwnerUserId",
                table: "Ads");

            migrationBuilder.DropForeignKey(
                name: "FK_CarImages_Ads_AdvertismentId",
                table: "CarImages");

            migrationBuilder.DropIndex(
                name: "IX_Ads_OwnerUserId",
                table: "Ads");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CarImages",
                table: "CarImages");

            migrationBuilder.DropColumn(
                name: "PhoneNumber",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "OwnerUserId",
                table: "Ads");

            migrationBuilder.RenameIndex(
                name: "IX_CarImages_AdvertismentId",
                table: "CarImages",
                newName: "IX_CarImage_AdvertismentId");

            migrationBuilder.AlterColumn<string>(
                name: "TransmissionTypes",
                table: "Ads",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "FuelType",
                table: "Ads",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<string>(
                name: "Owner",
                table: "Ads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhoneNumber",
                table: "Ads",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_CarImage",
                table: "CarImages",
                column: "CarImageId");

            migrationBuilder.AddForeignKey(
                name: "FK_CarImage_Ads_AdvertismentId",
                table: "CarImages",
                column: "AdvertismentId",
                principalTable: "Ads",
                principalColumn: "AdvertismentId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
