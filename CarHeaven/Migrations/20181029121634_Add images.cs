namespace CarHeaven.Web.Migrations
{
    using Microsoft.EntityFrameworkCore.Migrations;

    public partial class Addimages : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Images",
                table: "Ads",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Images",
                table: "Ads");
        }
    }
}
