namespace CarHeaven.Web.Migrations
{
    using Microsoft.EntityFrameworkCore.Migrations;

    public partial class Makepricedouble : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "CarCost",
                table: "Ads",
                nullable: false,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "CarCost",
                table: "Ads",
                nullable: false,
                oldClrType: typeof(double));
        }
    }
}
