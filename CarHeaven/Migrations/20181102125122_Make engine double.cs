namespace CarHeaven.Web.Migrations
{
    using Microsoft.EntityFrameworkCore.Migrations;

    public partial class Makeenginedouble : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "Engine",
                table: "Ads",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CarCost",
                table: "Ads",
                nullable: false,
                oldClrType: typeof(double));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Engine",
                table: "Ads",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<double>(
                name: "CarCost",
                table: "Ads",
                nullable: false,
                oldClrType: typeof(int));
        }
    }
}
