namespace CarHeaven.Web.Migrations
{
    using Microsoft.EntityFrameworkCore.Metadata;
    using Microsoft.EntityFrameworkCore.Migrations;

    public partial class Addnewtableforimages : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Images",
                table: "Ads");

            migrationBuilder.CreateTable(
                name: "CarImage",
                columns: table => new
                {
                    CarImageId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Link = table.Column<string>(nullable: true),
                    AdvertismentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CarImage", x => x.CarImageId);
                    table.ForeignKey(
                        name: "FK_CarImage_Ads_AdvertismentId",
                        column: x => x.AdvertismentId,
                        principalTable: "Ads",
                        principalColumn: "AdvertismentId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CarImage_AdvertismentId",
                table: "CarImage",
                column: "AdvertismentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CarImage");

            migrationBuilder.AddColumn<string>(
                name: "Images",
                table: "Ads",
                nullable: true);
        }
    }
}
