namespace CarHeaven.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using CarHeaven.Models;
    using CarHeaven.Models.ViewModels;
    using CarHeaven.Services.Interfaces.Interfaces;
    using CarHeaven.Services.Services;
    using CarHeaven.Web.Config;
    using CarHeaven.Web.Helpers;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Http.Internal;
    using Microsoft.AspNetCore.Mvc;

    public class AdvertismentsController : Controller
    {
        private readonly IAdvertismentsServices advertismentsServices;
        private readonly IImagesServices imagesServices;
        private readonly IViewsServices viewsServices;

        public AdvertismentsController(IAdvertismentsServices advertismentsServices, IImagesServices imagesServices, IViewsServices viewsServices)
        {
            this.advertismentsServices = advertismentsServices;
            this.imagesServices = imagesServices;
            this.viewsServices = viewsServices;
        }

        [HttpGet]
        public AdCardModel GetAdvert(int id)
        {
            string visitorId = HttpContext.Request.Cookies["Visitor" + id];
            if (visitorId == null)
            {
                HttpContext.Response.Cookies.Append("Visitor" + id, Guid.NewGuid().ToString(), new CookieOptions { HttpOnly = true, Secure = false, MaxAge = TimeSpan.FromDays(1) });
                viewsServices.CreateView(id);

                return advertismentsServices.GetAd(id);
            }

            return advertismentsServices.GetAd(id);
        }

        [HttpDelete]
        public bool DeleteAdvert(int id)
        {
            return advertismentsServices.DeleteAd(id);
        }

        [HttpPost]
        public List<AdCardModel> GetUserAds([FromBody] UserAdsModel userAds)
        {
            return advertismentsServices.GetUserAds(userAds);
        }

        [HttpGet]
        public int GetUserAdsNumber(int id)
        {
            return advertismentsServices.GetUserAdsNumber(id);
        }

        [HttpPost]
        public int GetSearchNumber([FromBody] GetAdsModel adsInfo)
        {
            return advertismentsServices.GetSearchAdsNumber(adsInfo);
        }

        [HttpPost]
        public List<AdCardModel> SearchAds([FromBody] GetAdsModel adsInfo)
        {
            return advertismentsServices.SearchAds(adsInfo);
        }

        [HttpPost]
        [Authorize]
        public IActionResult CreateAd([FromBody] PostAdModel ad)
        {
            if (ModelState.IsValid)
            {
                advertismentsServices.CreateAd(ad, HttpContext.User.Identity.Name);
                return Ok();
            }

            return BadRequest(ModelState);
        }

        [HttpPost]
        [Authorize]
        public IActionResult UpdateAd([FromBody] UpdateAdModel ad)
        {
            if (ModelState.IsValid)
            {
                advertismentsServices.UpdateAd(ad, HttpContext.User.Identity.Name);
                return Ok();
            }

            return BadRequest(ModelState);
        }

        [HttpPost]
        public async Task<string> UploadImage(IFormFile file)
        {
            var image = file as FormFile;
            var link = await imagesServices.UploadImageAsync(image, AppConfig.AWSAccessKey, AppConfig.AWSSecretKey, AppConfig.BucketName).ConfigureAwait(false);

            return link;
        }
    }
}
