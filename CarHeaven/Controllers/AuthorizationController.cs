namespace CarHeaven.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.Encodings.Web;
    using System.Threading.Tasks;
    using CarHeaven.Common;
    using CarHeaven.Models.ViewModels;
    using CarHeaven.Services.Interfaces.Interfaces;
    using CarHeaven.Services.Services;
    using CarHeaven.Web;
    using CarHeaven.Web.Config;
    using CarHeaven.Web.Helpers;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Localization;

    public class AuthorizationController : Controller
    {
        private const string EmailAction = "ConfirmEmail";
        private const string EmailController = "Authorization";

        private readonly IUsersServices usersServices;
        private readonly IHttpContextAccessor accessor;
        private readonly IEmailsServices emailsServices;
        private readonly IResources resources;

        public AuthorizationController(IUsersServices usersServices, IEmailsServices emailsServices, IHttpContextAccessor accessor, IResources resources)
        {
            this.usersServices = usersServices;
            this.accessor = accessor;
            this.emailsServices = emailsServices;
            this.resources = resources;
        }

        [HttpPost]
        public IActionResult RegisterUser([FromBody]CreateUserModel createUser)
        {
            if (ModelState.IsValid)
            {
                if (usersServices.Exist(createUser.Email))
                {
                    return Conflict();
                }

                var link = usersServices.CreateUser(createUser);

                if (createUser.RoleId.HasValue && createUser.RoleId.Value == Role.User)
                {
                    var callbackUrl = Url.Action(
                        EmailAction,
                        EmailController,
                        new { id = link.Guid },
                        Request.Scheme);
                    emailsServices.SendEmailAsync(
                        createUser.Email,
                        resources.EmailTitle,
                        string.Format(resources.EmailContent, HtmlEncoder.Default.Encode(callbackUrl)),
                        AppConfig.ServiceEmail,
                        AppConfig.ServiceOwner);
                }

                return Ok();
            }

            return BadRequest(ModelState);
        }

        [HttpGet]
        [Route("confirm-email")]
        public async Task<IActionResult> ConfirmEmail(string id)
        {
            if (usersServices.ConfirmEmail(id))
            {
                var user = usersServices.GetUserByGuid(id);
                await AuthorizationHelper.SignInAsync(accessor.HttpContext, user)
                    .ConfigureAwait(false);
                usersServices.CheckUpAuthorization(user.Email);

                return View("Index");
            }

            return RedirectToAction("InvalidConfirmation", new { id });
        }

        [Route("invalid-link")]
        public IActionResult InvalidConfirmation()
        {
            return View("Index");
        }

        [HttpGet]
        public IActionResult Renew(string id)
        {
            var link = usersServices.RenewLink(id) ?? new LinkModel();
            if (string.IsNullOrEmpty(link.Guid))
            {
                return NotFound();
            }

            var callbackUrl = Url.Action(
                EmailAction,
                EmailController,
                new { id = link.Guid },
                Request.Scheme);
            emailsServices.SendEmailAsync(
                usersServices.GetUser(link.UserId).Email,
                resources.EmailTitle,
                string.Format(resources.EmailContent, HtmlEncoder.Default.Encode(callbackUrl)),
                AppConfig.ServiceEmail,
                AppConfig.ServiceOwner);

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> LogIn([FromBody]LogInModel user)
        {
            var userInfo = usersServices.GetUserInfo(user.Email) ?? new CheckUserModel();
            if (userInfo.Status == UserStatus.Blocked)
            {
                return ValidationProblem();
            }

            if (userInfo.IsEmailConfirmed && await AuthorizationHelper.SignInAsync(accessor.HttpContext, user, userInfo).ConfigureAwait(false))
            {
                usersServices.CheckUpAuthorization(user.Email);

                return Ok(usersServices.GetUserBasicInfo(user.Email));
            }

            return Unauthorized();
        }

        [HttpPost]
        public async Task<IActionResult> LogInAdmin([FromBody]LogInModel user)
        {
            var userInfo = usersServices.GetUserInfo(user.Email) ?? new CheckUserModel();
            if (userInfo.Status == UserStatus.Blocked)
            {
                return ValidationProblem();
            }

            if (userInfo.IsEmailConfirmed && await AuthorizationHelper.SignInAsAdminAsync(accessor.HttpContext, user, userInfo).ConfigureAwait(false))
            {
                usersServices.CheckUpAuthorization(user.Email);

                return Ok(usersServices.GetUserBasicInfo(user.Email));
            }

            return Unauthorized();
        }

        [HttpGet]
        [Authorize]
        public ActionResult<UserResultModel> CheckAuthorization()
        {
            var userInfo = usersServices.GetUserBasicInfo(accessor.HttpContext.User.Claims.First().Value);

            return userInfo;
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> SignOutAsync()
        {
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                await AuthorizationHelper.SignOutAsync(accessor.HttpContext).ConfigureAwait(false);
            }

            return Ok();
        }
    }
}
