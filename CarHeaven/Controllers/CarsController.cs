namespace CarHeaven.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using CarHeaven.Models.ViewModels;
    using CarHeaven.Services.Interfaces.Interfaces;
    using Microsoft.AspNetCore.Mvc;

    public class CarsController : Controller
    {
        private readonly ICarsServices carsServices;

        public CarsController(ICarsServices carsServices)
        {
            this.carsServices = carsServices;
        }

        [HttpGet]
        public List<BrandModel> GetAllBrands()
        {
            return carsServices.GetAllBrands();
        }

        [HttpGet]
        public List<CarModelModel> GetModels(int brandId)
        {
            return carsServices.GetModels(brandId);
        }

        [HttpGet]
        public List<CarTypeModel> GetTypes()
        {
            return carsServices.GetTypes();
        }
    }
}
