namespace Vue.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using CarHeaven.Entities.Entities;
    using CarHeaven.Models;
    using CarHeaven.Models.ViewModels;
    using CarHeaven.Services;
    using CarHeaven.Services.Interfaces;
    using CarHeaven.Services.Interfaces.Interfaces;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Identity.UI.Pages.Account.Internal;
    using Microsoft.AspNetCore.Mvc;

    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
