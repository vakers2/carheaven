namespace CarHeaven.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using CarHeaven.Models.ViewModels;
    using CarHeaven.Services.Interfaces.Interfaces;
    using CarHeaven.Web.Helpers;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    public class UsersController : Controller
    {
        private readonly IUsersServices usersServices;
        private readonly IHttpContextAccessor accessor;

        public UsersController(IUsersServices usersServices, IHttpContextAccessor accessor)
        {
            this.usersServices = usersServices;
            this.accessor = accessor;
        }

        [HttpPost]
        [Authorize]
        public UsersSearchResultModel GetAllUsers([FromBody] GetUsersModel searchInfo)
        {
            return usersServices.GetAllUsersInfo(searchInfo);
        }

        [HttpPost]
        [Authorize]
        public IActionResult ChangeStatus([FromBody] ChangeStatusModel changeStatusInfo)
        {
            usersServices.ChangeUsersStatus(changeStatusInfo);

            return Ok();
        }

        [HttpPost]
        [Authorize]
        public async Task<bool> UpdateUserAsync([FromBody]UpdateUserModel user)
        {
            var result = usersServices.UpdateUser(user, accessor.HttpContext.User.Identity.Name);
            if (result)
            {
                await AuthorizationHelper.SignOutAsync(accessor.HttpContext).ConfigureAwait(false);
                await AuthorizationHelper.SignInAsync(accessor.HttpContext, new CheckUserModel { Email = user.Email }).ConfigureAwait(false);
            }

            return result;
        }
    }
}
