﻿namespace CarHeaven.Repository.Interfaces.Interfaces
{
    using System.Collections.Generic;
    using CarHeaven.Entities;
    using CarHeaven.Models;
    using CarHeaven.Models.ViewModels;

    public interface IAdvertismentsRepository
    {
        void InsertAd(Advertisment ad);

        List<Advertisment> GetUserAds(int id, int page = 1);

        int GetUserAdsCount(int id);

        List<Advertisment> GetSearchAds(GetAdsModel adsInfo);

        int GetSearchAdsCount(GetAdsModel adsInfo);

        Advertisment GetAd(int id);

        void DeleteAd(Advertisment advert);

        void UpdateAd(Advertisment advert);
    }
}
