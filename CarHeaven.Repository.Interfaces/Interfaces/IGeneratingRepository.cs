﻿namespace CarHeaven.Repository.Interfaces.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using CarHeaven.Entities;

    public interface IGeneratingRepository
    {
        void SeedAds();

        CarModel CreateCar(string brand, string model, string carType);
    }
}
