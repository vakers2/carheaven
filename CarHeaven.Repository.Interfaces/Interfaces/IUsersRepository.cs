﻿namespace CarHeaven.Repository.Interfaces.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using CarHeaven.Entities.Entities;
    using CarHeaven.Models.ViewModels;

    public interface IUsersRepository
    {
        RegistrationLink CreateUser(User user);

        User GetUser(int userId);

        User GetUser(string email);

        User GetUserByGuid(string guid);

        List<User> GetAllUsers(GetUsersModel info);

        int GetAllUsersCount(GetUsersModel info);

        RegistrationLink RenewLink(RegistrationLink link);

        void ConfirmEmail(RegistrationLink link);

        void CheckUpAuthorization(string email);

        bool UpdateUser(User user);

        RegistrationLink GetLink(string id);
    }
}
