﻿namespace CarHeaven.Repository.Interfaces.Interfaces
{
    using System.Collections.Generic;
    using CarHeaven.Entities;

    public interface ICarsRepository
    {
        List<Brand> GetAllBrands();

        List<CarModel> GetModels(int brandId);

        List<CarType> GetAllTypes();

        CarModel GetModelById(int id);
    }
}
