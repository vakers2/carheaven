﻿namespace CarHeaven.Repository.Interfaces.Interfaces
{
    public interface IViewsRepository
    {
        void InsertView(int id);
    }
}