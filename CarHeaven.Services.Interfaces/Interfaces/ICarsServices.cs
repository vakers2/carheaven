﻿namespace CarHeaven.Services.Interfaces.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using CarHeaven.Models.ViewModels;

    public interface ICarsServices
    {
        List<BrandModel> GetAllBrands();

        List<CarModelModel> GetModels(int brandId);

        List<CarTypeModel> GetTypes();
    }
}
