﻿namespace CarHeaven.Services.Interfaces.Interfaces
{
    public interface IViewsServices
    {
        void CreateView(int id);
    }
}