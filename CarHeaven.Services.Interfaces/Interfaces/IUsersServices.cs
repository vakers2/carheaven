﻿namespace CarHeaven.Services.Interfaces.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using CarHeaven.Entities.Entities;
    using CarHeaven.Models.ViewModels;
    using Microsoft.AspNetCore.Http;

    public interface IUsersServices
    {
        LinkModel CreateUser(CreateUserModel createUser);

        CreateUserModel GetUser(int userId);

        CreateUserModel GetUser(string email);

        UserResultModel GetUserBasicInfo(string email);

        CheckUserModel GetUserInfo(string email);

        CheckUserModel GetUserByGuid(string guid);

        UsersSearchResultModel GetAllUsersInfo(GetUsersModel info);

        bool Exist(string email);

        LinkModel RenewLink(string guid);

        bool ConfirmEmail(string guid);

        void CheckUpAuthorization(string email);

        bool UpdateUser(UpdateUserModel userInfo, string originalEmail);

        void ChangeUsersStatus(ChangeStatusModel statusInfo);
    }
}
