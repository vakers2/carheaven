﻿namespace CarHeaven.Services.Interfaces.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http.Internal;

    public interface IImagesServices
    {
        Task<string> UploadImageAsync(FormFile image, string accessKey, string secretKey, string bucketName);
    }
}
