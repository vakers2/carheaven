﻿namespace CarHeaven.Services.Interfaces.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;

    public interface IEmailsServices
    {
        Task ExecuteAsync(string apiKey, string subject, string message, string email, string serviceEmail, string owner);

        Task SendEmailAsync(string email, string subject, string message, string serviceEmail, string owner);
    }
}
