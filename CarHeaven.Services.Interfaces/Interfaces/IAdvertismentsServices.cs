﻿namespace CarHeaven.Services.Interfaces.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using CarHeaven.Entities;
    using CarHeaven.Models;
    using CarHeaven.Models.ViewModels;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Http.Internal;

    public interface IAdvertismentsServices
    {
        AdCardModel GetAd(int id);

        List<AdCardModel> GetUserAds(UserAdsModel userAds);

        int GetUserAdsNumber(int id);

        void CreateAd(PostAdModel ad, string email);

        List<AdCardModel> SearchAds(GetAdsModel adsInfo);

        int GetSearchAdsNumber(GetAdsModel adsInfo);

        bool DeleteAd(int id);

        void UpdateAd(UpdateAdModel ad, string email);
    }
}
