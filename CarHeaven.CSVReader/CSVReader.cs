﻿using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using CarHeaven.Models;

namespace CarHeaven.CsvReader
{
    public static class CsvReader
    {
        public static List<AdCardModel> ReadCsv(string path)
        {
            FileInfo file = new FileInfo(path);
            List<AdCardModel> carList = new List<AdCardModel>();
            using (StreamReader reader = new StreamReader(file.FullName))
            {
                string line;
                reader.ReadLine();
                while ((line = reader.ReadLine()) != null)
                {
                    string[] csvColumn = line.Split(',');
                    carList.Add(new AdCardModel
                    {
                        Brand = csvColumn[0],
                        CarModel = csvColumn[1],
                        CarType = csvColumn[2]
                    });
                }
            }

            return carList;
        }
    }
}
