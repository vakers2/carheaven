﻿namespace CarHeaven.Common
{
    public enum TransmissionTypes
    {
        Automatic = 0,
        Manual = 1,
        SemiAutomatic = 2
    }

}
