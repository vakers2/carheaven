﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarHeaven.Common
{
    public enum SortFieldType
    {
        EditDate = 0,
        Year = 1,
        Price = 2
    }
}
