﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarHeaven.Common
{
    public enum Role
    {
        User = 1,
        Admin = 2,
        SuperAdmin = 3
    }
}
