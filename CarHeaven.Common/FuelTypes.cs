﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarHeaven.Common
{
    public enum FuelTypes
    {
        Gasoline = 0,
        Diesel = 1,
        Gas = 2,
        Electricity = 3
    }
}
