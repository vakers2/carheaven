﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarHeaven.Common
{
    public enum UserStatus
    {
        Active = 0,
        AwaitingEmailConfirmation = 1,
        Blocked = 2
    }
}
