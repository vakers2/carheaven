namespace CarHeaven.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using CarHeaven.Common;
    using CarHeaven.Entities.Entities;

    public class Advertisment
    {
        public int AdvertismentId { get; set; }

        public CarModel CarModel { get; set; }

        public User Owner { get; set; }

        public string Description { get; set; }

        public int CarYear { get; set; }

        public double CarCost { get; set; }

        public FuelTypes FuelType { get; set; }

        public double Engine { get; set; }

        public int Mileage { get; set; }

        public string CarColor { get; set; }

        public TransmissionTypes Transmission { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime ExpireDate { get; set; }

        public List<CarImage> Images { get; set; }

        public List<View> Views { get; set; }
    }
}
