﻿namespace CarHeaven.Entities.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class CarImage
    {
        public int CarImageId { get; set; }

        public string Link { get; set; }

        public int AdvertismentId { get; set; }

        public Advertisment Advertisment { get; set; }
    }
}
