namespace CarHeaven.Entities
{
    using System;
    using System.Collections.Generic;

    public class CarType
    {
        public int CarTypeId { get; set; }

        public string CarTypeName { get; set; }

        public List<CarModel> Models { get; set; }
    }
}
