namespace CarHeaven.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class Brand
    {
        public int BrandId { get; set; }

        public string BrandName { get; set; }

        public List<CarModel> Models { get; set; }
    }
}
