﻿namespace CarHeaven.Entities.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class View
    {
        public int ViewId { get; set; }

        public DateTime ViewDate { get; set; }

        public int AdvertismentId { get; set; }

        public Advertisment Advertisment { get; set; }
    }
}
