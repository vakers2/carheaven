﻿namespace CarHeaven.Entities.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Text;
    using CarHeaven.Common;

    public class User
    {
        public int UserId { get; set; }

        [MaxLength(128)]
        [DataType(DataType.EmailAddress)]
        [Required]
        public string Email { get; set; }

        [MaxLength(64)]
        [Required]
        public string Password { get; set; }

        [MaxLength(128)]
        [Required]
        public string FirstName { get; set; }

        [MaxLength(128)]
        [Required]
        public string LastName { get; set; }

        [Required]
        public string PhoneNumber { get; set; }

        public DateTime? LastLoginDate { get; set; }

        public DateTime DateCreated { get; set; }

        public bool IsEmailConfirmed { get; set; }

        [DefaultValue(1)]
        public Role RoleId { get; set; }

        public List<Advertisment> Advertisments { get; set; }

        [DefaultValue(1)]
        public UserStatus Status { get; set; }
    }
}
