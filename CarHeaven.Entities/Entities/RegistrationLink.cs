﻿namespace CarHeaven.Entities.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class RegistrationLink
    {
        public int RegistrationLinkId { get; set; }

        public string Guid { get; set; }

        public DateTime ExpireDate { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }
    }
}
