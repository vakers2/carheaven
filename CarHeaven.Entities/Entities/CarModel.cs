namespace CarHeaven.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class CarModel
    {
        public int CarModelId { get; set; }

        public string CarModelName { get; set; }

        public int BrandId { get; set; }

        public Brand Brand { get; set; }

        public int CarTypeId { get; set; }

        public CarType CarType { get; set; }
    }
}
