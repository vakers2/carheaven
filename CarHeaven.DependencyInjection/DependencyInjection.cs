﻿namespace CarHeaven.DependencyInjection
{
    using System;
    using System.Linq;
    using CarHeaven.Repository.Repository;
    using CarHeaven.Services.Services;
    using Microsoft.Extensions.DependencyInjection;

    public static class DependencyInjection
    {
        public static void InjectDependencies(IServiceCollection services)
        {
            RegisterScoped(typeof(AdvertismentsServices), "Services", services);
            RegisterScoped(typeof(AdvertismentsRepository), "Repository", services);
        }

        private static void RegisterScoped(Type anyType, string typesPostfix, IServiceCollection services)
        {
            var types = anyType.Assembly.GetExportedTypes().Where(x => x.IsClass && !x.IsAbstract && x.Name.EndsWith(typesPostfix));

            foreach (var type in types)
            {
                var interfaceType = type.GetInterface("I" + type.Name);
                services.AddScoped(interfaceType, type);
            }
        }
    }
}
